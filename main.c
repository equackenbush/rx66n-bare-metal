#include <platform.h>

void main(void)
{
	PORT3.PODR.BIT.B2 = 0;	// configure pin for low output
	PORT3.ODR0.BIT.B6 = 0;	// configure pin for CMOS output
	PORT3.PMR.BIT.B2 = 0;	// configure pin as GPIO
	PORT3.PDR.BIT.B2 = 1;	// configure pin as output

	PORT3.PODR.BIT.B2 = 1;	// start with LED off

	while(1)
	{
		PORT3.PODR.BIT.B2 = ~PORT3.PODR.BIT.B2;	// toggle LED

		R_BSP_SoftwareDelay(500, BSP_DELAY_MILLISECS);
	}
}
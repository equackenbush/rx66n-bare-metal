CFLAGS = -isa=rxv3 -fpu -dpfpu -asmopt=-bank -lang=c99 -utf8 -nomessage -output=obj -outcode=utf8 -nologo
INCLUDES = -include="C:\Program Files (x86)\Renesas\RX\3_6_0\include,r_bsp,r_config"
TARGET = rx66n-bare-metal
BUILD_DIR = build
LBGRX_FLAGS = -isa=rxv3 -fpu -dpfpu -lang=c99 -head=runtime,stdio,stdlib,string,new -nologo
LD_FLAGS = -form=hexadecimal -start=SU,SI,B_1,R_1,B_2,R_2,B,R,B_8,R_8/04,PResetPRG,C_1,C_2,C,C_8,C$$\*,D\*,W\*,L,P/0FFC00000,EXCEPTVECT/0FFFFFF80,RESETVECT/0FFFFFFFC,BEXRAM_1,REXRAM_1,BEXRAM_2,REXRAM_2,BEXRAM,REXRAM,BEXRAM_8,REXRAM_8/00800000 -lib="$(BUILD_DIR)/$(TARGET).lib"
SRC = $(wildcard r_bsp/board/generic_rx66n/*.c) \
	$(wildcard r_bsp/mcu/all/*.c) \
	$(wildcard r_bsp/mcu/rx66n/*.c) \
	$(wildcard *.c)

all: compile lib link

link: $(wildcard $(BUILD_DIR)/*obj)
	rlink $(LD_FLAGS) $^

lib:
	lbgrx $(LBGRX_FLAGS) -output="$(BUILD_DIR)/$(TARGET).lib"

compile: $(SRC)
	@echo 'Compiling: $<'
	ccrx $(CFLAGS) $(INCLUDES) -obj_path="$(BUILD_DIR)" $^